public class Wrapper{

    public static void main(String[] args) {
        
        Integer numero = Integer.valueOf(1); // tipo primitivo dentro de uma variavel (wrapper); - autoboxing
        int soma = numero; //  tipo wrapper dentro de um objeto primitivo; - unboxing
            
            System.out.println(soma);


        Boolean resposta = (true);
        boolean pergunta = resposta;   
            
            System.out.println(pergunta);

        Character mentorama = 'M'; // usado com aspas simples
        char primeiraLetra = mentorama;

            System.out.println(primeiraLetra);

        Float x = (3.1415926f);   
        float pi = x;
        
            System.out.println(pi);

        Double senhaCadeado = (20.2122);
        double abrirCadeado = senhaCadeado;
        
            System.out.println(abrirCadeado);

        Long longo = (212121212121212121l);
        long doisUm = longo;

            System.out.println(doisUm);

        Byte numero0 =  (0); 
        byte numero1  = (1);

            System.out.println(numero0 + numero1);
            System.out.println(numero0 * numero1);

         Short sequencia = 12345;
         short numeros = sequencia;
         
         System.out.println(numeros);

         
           









            
    }
}
